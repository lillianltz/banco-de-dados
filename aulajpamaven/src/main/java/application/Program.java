package application;

import domain.Person;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Program {

	public static void main(String[] args) {
		
		/*Person p1 = new Person(null, "Lillian", "lillian@gmail.com");
		Person p2 = new Person(null, "André", "lillian@gmail.com");
		Person p3 = new Person(null, "Benjamim", "lillian@gmail.com");*/
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("exemplo-jpa");
		EntityManager em = emf.createEntityManager();
		
		
		Person p01 = em.find(Person.class, 1);
		System.out.println(p01);
		
		Person p = em.find(Person.class, 6);
		
		em.getTransaction().begin();
		em.remove(p);
		em.getTransaction().commit();

		/*em.getTransaction().begin();
		em.persist(p1);
		em.persist(p2);
		em.persist(p3);
		em.getTransaction().commit();*/
		System.out.println("Pronto!");
		
		em.close();
		emf.close();
		
		
	}

}
